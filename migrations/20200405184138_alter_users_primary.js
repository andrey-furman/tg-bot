exports.up = function (knex) {
  return Promise.all([
    knex.schema.alterTable('users', t => {
      t.dropColumn('user_id')
      t.dropColumn('phone')
    }),
  ])
}

exports.down = function (knex) {}
