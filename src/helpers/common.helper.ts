import axios from 'axios'
import moment from 'moment'
import dropboxV2Api from 'dropbox-v2-api'

import { session } from '../bootstrap'
import {
  Contexts,
  ContextMessageUpdate as Ctx,
  ProposalKeyboardData,
  InlineKeyboardButton,
} from '../shared'
import { inlineKeyboards } from '../helpers'
import papyrus from '../papyrus'

const dropbox = dropboxV2Api.authenticate({
  token: process.env.DROPBOX_TOKEN,
})

interface UserData {
  username: string
  telegramId: number
  phone: number
  type: string
}

interface Region {
  region_id: number
  region: string
}

// todo optimize type recognition for numbers
export const getIdAndCbData = (ctx: Ctx): (number | string)[] => {
  const cb = ctx.callbackQuery.data
  const [cbPrefix, cbData] = cb.split(':')
  const {
    from: { id },
  } = ctx

  return [id, cbData]
}

export const getIdAndQueryParam = (ctx: Ctx): (number | string)[] => {
  const {
    from: { id },
  } = ctx
  const queryRaw = ctx.inlineQuery.query
  const [query, param] = queryRaw.split(':')

  return [id, param]
}

export const createUser = (userData: UserData) => {}

export interface InlineQueryListItem {
  id: string
  type: string
  photo_url: string
  thumb_url: string
  title: string
  description: string
  reply_markup: {
    inline_keyboard: Array<any>
  }
  input_message_content: {
    message_text: string
    parse_mode: string
  }
}

export const getQueryList = (items: Region[]): InlineQueryListItem[] => {
  const listItems = []
  items.map(({ region_id, region }) => {
    listItems.push({
      id: String(region_id),
      type: 'article',
      photo_url: '',
      thumb_url: '',
      title: region,
      description: 'Выберите область',
      reply_markup: {
        inline_keyboard: inlineKeyboards.outcomeCurrencies,
      },
      input_message_content: {
        message_text: region,
        parse_mode: 'markdown',
      },
    })
  })

  return listItems
}

export const dropboxUpload = (filePath: string, fileName: string) => {
  return new Promise(async (resolve, reject) => {
    let publicLink: string | boolean
    const rs = await axios.get(filePath, {
      responseType: 'stream',
    })
    const ws = await dropbox(
      {
        resource: 'files/upload',
        parameters: {
          path: `/vt-photos/${fileName}`,
        },
      },
      async (err, result) => {
        if (err) {
          reject(err)
        } else {
          publicLink = await getDropboxPublicLink(result.path_lower)
          resolve(publicLink)
        }
      },
    )
    rs.data.pipe(ws)
  })
}

export const getFilePath = async (id: string): Promise<string[]> => {
  const resultGetFile = await axios.get(`${process.env.TELEGRAM_BASE_URL}/getFile`, {
    params: { file_id: id },
  })

  if (!resultGetFile.data.ok) {
    throw new Error('err on receiving file from telegram.')
  }

  const filePath = resultGetFile.data.result.file_path
  const splitedFilePath = filePath.split('/')
  const fileName = splitedFilePath[splitedFilePath.length - 1]
  const resultPath = `${process.env.TELEGRAM_FILE_URL}/${filePath}`

  return [resultPath, fileName]
}

const getDropboxPublicLink = async (path: string): Promise<string | boolean> => {
  try {
    const endpoint = 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings'
    const data = {
      path,
      settings: {
        requested_visibility: 'public',
      },
    }
    const requestResult = await axios.post(endpoint, data, {
      headers: {
        Authorization: `Bearer ${process.env.DROPBOX_TOKEN}`,
        'Content-Type': 'application/json',
      },
    })

    return requestResult.data.url.replace('https://www', 'https://dl')
  } catch (err) {
    if (err.response.status === 409) {
      return false
    }
    throw new Error(papyrus.errors.uploadingFileError)
  }
}

export const getProposalCard = (
  items: any[],
  itemIndex: number,
  isProposalsMode: string,
): { photoUrl: string; inlineKeyboard: InlineKeyboardButton[][]; caption: string } | boolean => {
  if (items.length === 0) {
    return false
  }
  const curItem = items[itemIndex]
  const category = curItem.category || ''
  let itemId = curItem.item_id
  const phone = curItem.phone
  const body = curItem.body
  const weight = curItem.weight || 0
  const photoUrl = curItem.photo_url || papyrus.defaultProductImage
  const createdAt = moment(curItem.created_at).format('DD.MM.YYYY')
  const username = isProposalsMode ? curItem.username || '' : ''
  if (isProposalsMode) {
    itemId = 0
  }
  const keyboardData = prepareProposalKeyboardData(items, itemIndex)
  // on case inline button username is not available - add username to caption
  const caption = formCardCaption(body, weight || 0, phone, username, createdAt, category)
  const inlineKeyboard: InlineKeyboardButton[][] = inlineKeyboards.proposalCard(
    keyboardData,
    itemId,
  )

  return { photoUrl, inlineKeyboard, caption }
}

export const getCurrentCardKeyboard = async (
  id: number,
  session,
): Promise<InlineKeyboardButton[][]> => {
  const { cardItemIndex: cardItemIndexRaw, items: itemsRaw } = await session.getSession(id)
  const cardItemIndex = Number(cardItemIndexRaw)
  const items = JSON.parse(itemsRaw)
  const { ad_id } = items[cardItemIndex]
  const keyboardData = prepareProposalKeyboardData(items, cardItemIndex)
  const inlineKeyboard: InlineKeyboardButton[][] = inlineKeyboards.proposalCard(keyboardData, ad_id)

  return inlineKeyboard
}

export const prepareProposalKeyboardData = (
  items: any,
  itemIndex: number,
): ProposalKeyboardData => {
  const amount = items.length || 0
  const username = items[itemIndex].username || ''
  let prevId = 0
  let nextId = 0
  const keyboardData: ProposalKeyboardData = {
    username,
    cur: { position: `${itemIndex + 1}/${amount}` },
    next: { index: prevId },
    prev: { index: nextId },
    isFirst: true,
    isLast: true,
  }
  if (itemIndex !== 0) {
    keyboardData.isFirst = false
  }
  if (itemIndex !== items.length - 1) {
    keyboardData.isLast = false
  }
  keyboardData.prev.index = itemIndex - 1
  keyboardData.next.index = itemIndex + 1

  return keyboardData
}

export const formCardCaption = (
  description: string,
  weight: number,
  phone: string,
  username: string,
  createdAt: string,
  category?: string,
): string => {
  let caption = `${description}\nВес: ${weight}тонн(ы)\nТелефон: ${phone}`
  if (username) {
    caption += `\n${papyrus.writeToUser} @${username}`
  }
  if (category) {
    caption += `\nКатегория: ${category}`
  }
  caption += `\nДата публикации: ${createdAt}`

  return caption
}

export const updateSessionAndOutputProposalCards = async (
  ctx: Ctx,
  items: any,
  categoryId: number,
) => {
  const {
    from: { id },
  } = ctx
  const { isProposalsMode } = await session.getSession(id)
  await session.update(id, {
    items: JSON.stringify(items),
    itemId: 0,
    categoryId,
    context: Contexts.onMainMenu,
  })
  const cardsData: any = getProposalCard(items, 0, isProposalsMode)
  if (!cardsData) {
    return await ctx.reply(papyrus.absentProposals)
  }
  const { photoUrl, inlineKeyboard, caption } = cardsData
  const itemUrl = photoUrl || papyrus.defaultProductImage
  await ctx.reply(papyrus.cardUsingInstruction)

  return await ctx.telegram.sendPhoto(id, itemUrl, {
    reply_markup: {
      inline_keyboard: inlineKeyboard,
    },
    caption,
    parse_mode: 'HTML',
  })
}
