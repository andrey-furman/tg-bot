import {
  Callbacks,
  Buttons,
  InlineQueries,
  Category,
  ProposalKeyboardData,
  InlineKeyboardButton,
} from '../../shared'
import papyrus from '../../papyrus'

export const inlineKeyboards = {
  outcomeCurrencies: (): InlineKeyboardButton[][] => {
    return [[{ text: Buttons.toMenu, callback_data: `cb` }]]
  },
  clientType: (): InlineKeyboardButton[][] => {
    return [
      [
        {
          text: Buttons.seller,
          switch_inline_query_current_chat: `${InlineQueries.onClientTypeSelected}:seller`,
        },
        { text: Buttons.buyer, callback_data: `${Callbacks.onClientTypeSelected}:buyer` },
      ],
    ]
  },
  confirmationKeyboard: (): InlineKeyboardButton[][] => {
    return [
      [
        { text: papyrus.confirm, callback_data: `${Callbacks.onConfirmAction}` },
        { text: papyrus.decline, callback_data: `${Callbacks.onDeclineAction}` },
      ],
    ]
  },
  categories: (categories: Category[], inlineMode = false): InlineKeyboardButton[][] => {
    const keyboard = []
    let catsRow = []

    categories.map((category, i) => {
      const { category: categoryName, category_id: categoryId } = category
      if (catsRow.length === 2 || categories.length === i + 1) {
        keyboard.push(catsRow)
        catsRow = []
      }
      let action: any = {
        callback_data: `${Callbacks.onSelectCategory}:${categoryId}`,
      }
      if (inlineMode) {
        action = {
          switch_inline_query_current_chat: `${InlineQueries.onEnterBuyerProposal}:${categoryId}`,
        }
      }
      catsRow.push({
        text: categoryName,
        ...action,
      })
    })

    return keyboard
  },
  proposalCard: (keyboardRaw: ProposalKeyboardData, itemId: number): InlineKeyboardButton[][] => {
    const prevCardBtn = {
      text: Buttons.previous,
      callback_data: `${Callbacks.onChangeCard}:${keyboardRaw.prev.index}`,
    }
    const nextCardBtn = {
      text: Buttons.next,
      callback_data: `${Callbacks.onChangeCard}:${keyboardRaw.next.index}`,
    }
    const keyboardData: InlineKeyboardButton[][] = [
      [{ text: keyboardRaw.cur.position, callback_data: 'empty' }],
    ]
    if (!keyboardRaw.isFirst) {
      keyboardData[0].unshift(prevCardBtn)
    }
    if (!keyboardRaw.isLast) {
      keyboardData[0].push(nextCardBtn)
    }
    if (itemId !== 0) {
      keyboardData.push([
        { text: papyrus.edit, callback_data: `${Callbacks.onEditItem}:${itemId}` },
        { text: papyrus.delete, callback_data: `${Callbacks.onDeleteItem}:${itemId}` },
      ])
    }

    return keyboardData
  },
  editMenu: (itemId: number, type: string): InlineKeyboardButton[][] => {
    const photoBtn = [
      { text: papyrus.editMenu.photo, callback_data: `${Callbacks.onEditPhoto}:${itemId}` },
    ]
    const keyboard = [
      [{ text: papyrus.editMenu.desc, callback_data: `${Callbacks.onEditDesc}:${itemId}` }],
      [{ text: papyrus.editMenu.weight, callback_data: `${Callbacks.onEditWeight}:${itemId}` }],
      [{ text: papyrus.editMenu.back, callback_data: `${Callbacks.back}` }],
    ]
    if (type === 'seller') {
      keyboard.unshift(photoBtn)
    }

    return keyboard
  },
}
