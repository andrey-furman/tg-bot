import Markup from 'telegraf/markup'

import { Handler } from './main.handler'
import { Ads, Proposals, Users, Regions, Categories } from '../../DB/models'
import { ContextMessageUpdate as Ctx, Callbacks, Contexts } from '../../shared'
import {
  keyboards,
  getIdAndCbData,
  getProposalCard,
  getQueryList,
  inlineKeyboards,
  getCurrentCardKeyboard,
  updateSessionAndOutputProposalCards,
} from '../../helpers'
import { knex } from '../../DB'
import { session } from '../../bootstrap'
import papyrus from '../../papyrus'

const scenarious = new Map()

scenarious.set(Callbacks.onClientTypeSelected, async (ctx: Ctx) => {
  const [id, type] = getIdAndCbData(ctx)
  const { telegramId, phone, firstName, username, chatId } = await session.getSession(id)
  const createRes = await Users.query().insert({
    telegram_id: Number(telegramId),
    first_name: firstName || '',
    username: username || '',
    region_id: 0,
    chat_id: Number(chatId) || 0,
    phone,
    type: 'buyer',
  })
  if (!createRes) {
    throw new Error(papyrus.errors.errOnCreateClient)
  }

  await session.update(id, { context: Contexts.onMainMenu, type })
  await ctx.editMessageReplyMarkup({ inline_keyboard: [] })
  return await ctx.reply(papyrus.menu, Markup.keyboard(keyboards.menu).resize().extra())
})

scenarious.set(Callbacks.onChangeCard, async (ctx: Ctx) => {
  const [id, itemIndexRaw] = getIdAndCbData(ctx)
  const itemIndex = Number(itemIndexRaw)
  await session.update(id, { cardItemIndex: itemIndex })
  const { items: itemsRaw, isProposalsMode } = await session.getSession(id)
  const items = JSON.parse(itemsRaw)
  await session.update(id, { itemId: items[itemIndex].item_id })
  const cardsData: any = getProposalCard(items, itemIndex || 0, isProposalsMode)
  if (!cardsData) {
    return await ctx.reply(papyrus.absentProposals)
  }
  const { photoUrl, inlineKeyboard, caption } = cardsData
  const photoLink = photoUrl || papyrus.defaultProductImage
  const media = { type: 'photo', media: photoLink, caption, parse_mode: 'Markdown' }

  return await ctx.editMessageMedia(media, { reply_markup: { inline_keyboard: inlineKeyboard } })
})

scenarious.set(Callbacks.onEditItem, async (ctx: Ctx) => {
  const [id, itemIdRaw] = getIdAndCbData(ctx)
  const itemId = Number(itemIdRaw)
  const { type } = await session.getSession(id)
  await session.update(id, {
    context: Contexts.onEditItemMenu,
    itemId: itemId,
  })

  return await ctx.reply(
    papyrus.edit,
    Markup.inlineKeyboard(inlineKeyboards.editMenu(itemId, type)).resize().extra(),
  )
})

scenarious.set(Callbacks.onConfirmAction, async (ctx: Ctx) => {
  const {
    update: {
      callback_query: {
        message: { message_id },
      },
    },
  } = ctx
  const [id, empty] = getIdAndCbData(ctx)
  const { type, itemId: itemIdRaw, phone, cardId: cardIdRaw } = await session.getSession(id)
  const itemId = Number(itemIdRaw)
  let updatedItems = []
  if (type === 'seller') {
    await Proposals.query().deleteById(itemId)
    updatedItems =
      (await Proposals.query()
        .select(
          knex.raw(
            'categories.category, proposals.proposal_id as item_id, proposals.body, proposals.phone, proposals.photo_url, proposals.created_at, proposals.weight',
          ),
        )
        .leftJoin('categories', 'categories.category_id', '=', 'proposals.category_id')
        .where('proposals.phone', '=', phone)
        .limit(100)) || []
  } else {
    await Ads.query().deleteById(itemId)
    updatedItems =
      (await Ads.query()
        .select(
          knex.raw(
            'categories.category, ads.ad_id as item_id, ads.body, ads.phone, ads.created_at, ads.weight',
          ),
        )
        .leftJoin('categories', 'categories.category_id', '=', 'ads.category_id')
        .where('ads.phone', '=', phone)
        .limit(100)) || []
  }
  await ctx.deleteMessage(message_id)
  await session.update(id, {
    context: Contexts.onMainMenu,
    itemId: '',
    items: JSON.stringify(updatedItems),
  })
  const cardsData: any = getProposalCard(updatedItems, 0, '')
  if (!cardsData) {
    return await ctx.reply(papyrus.absentProposals)
  }
  const { photoUrl, inlineKeyboard, caption } = cardsData
  await ctx.reply(papyrus.cardUsingInstruction)

  return await ctx.telegram.sendPhoto(id, photoUrl, {
    reply_markup: {
      inline_keyboard: inlineKeyboard,
    },
    caption,
    parse_mode: 'Markdown',
  })
})

scenarious.set(Callbacks.onDeclineAction, async (ctx: Ctx) => {
  const [id, empty] = getIdAndCbData(ctx)
  await session.update(id, { context: Contexts.onMainMenu, itemId: '', cardId: '' })
  const inlineKeyboard = await getCurrentCardKeyboard(Number(id), session)

  return await ctx.editMessageReplyMarkup({ inline_keyboard: inlineKeyboard })
})

scenarious.set(Callbacks.onEditPhoto, async (ctx: Ctx) => {
  const [id, empty] = getIdAndCbData(ctx)
  await session.update(id, { context: Contexts.onAdPhotoUploading })

  return await ctx.reply(papyrus.uploadPhoto)
})

scenarious.set(Callbacks.onEditWeight, async (ctx: Ctx) => {
  const [id, empty] = getIdAndCbData(ctx)
  await session.update(id, { context: Contexts.onEnterWeight })

  return await ctx.reply(papyrus.enterWeight)
})

scenarious.set(Callbacks.onEditDesc, async (ctx: Ctx) => {
  const [id, empty] = getIdAndCbData(ctx)
  const { type } = await session.getSession(id)
  let description = papyrus.enterDescriptionBuyer
  if (type === 'seller') {
    description = papyrus.enterDescriptionSeller
  }
  await session.update(id, { context: Contexts.onEnterDescription })

  return await ctx.reply(description)
})

scenarious.set(Callbacks.back, async (ctx: Ctx) => {
  const [id, empty] = getIdAndCbData(ctx)
  await session.update(id, { context: Contexts.onMainMenu, itemId: '', isProposalMode: '' })
  await ctx.editMessageReplyMarkup({ inline_keyboard: [] })

  return await ctx.reply(papyrus.menu)
})

scenarious.set(Callbacks.onDeleteItem, async (ctx: Ctx) => {
  const [id, itemId] = getIdAndCbData(ctx)
  await session.update(id, { itemId })
  return await ctx.editMessageReplyMarkup({
    inline_keyboard: inlineKeyboards.confirmationKeyboard(),
  })
})

scenarious.set(Callbacks.onSelectCategory, async (ctx: Ctx) => {
  const [id, categoryIdRaw] = getIdAndCbData(ctx)
  const categoryId = Number(categoryIdRaw)
  const { isProposalsMode, type } = await session.getSession(id)
  let condition: any
  let desc = papyrus.enterDescriptionSeller
  await ctx.editMessageReplyMarkup({ inline_keyboard: [] })
  if (isProposalsMode) {
    if (type === 'buyer') {
      const regions = await Regions.query().select().orderBy('region', 'asc')
      const regionsList: any = getQueryList(regions)
      await session.update(id, {
        type,
        categoryId,
        context: Contexts.onSelectSellerProposalsRegion,
      })
      return await ctx.answerInlineQuery(regionsList, {
        cache_time: 0,
      })
    } else {
      // proposals for seller
      await session.update(id, { context: Contexts.onSelectProposalRegion })
      condition = { field: 'ads.category_id', value: categoryId }
      const items = await Ads.query()
        .select(
          knex.raw(
            'users.telegram_id, users.username, ads.ad_id as item_id, ads.body, users.phone, ads.created_at, ads.weight, categories.category',
          ),
        )
        .leftJoin('users', 'users.phone', '=', 'ads.phone')
        .leftJoin('categories', 'categories.category_id', '=', 'ads.category_id')
        .where(condition.field, '=', condition.value)
        .limit(100)
      await updateSessionAndOutputProposalCards(ctx, items, categoryId)
    }
    // place an announcement
  } else {
    await session.update(id, { categoryId, context: Contexts.onEnterDescription })
    return await ctx.reply(desc)
  }
})

export class CbHandler extends Handler {
  constructor() {
    super()
  }

  handle(ctx: Ctx) {
    const cb = ctx.callbackQuery.data
    const [cbPrefix, ...rest] = cb.split(':')
    if (!Object.keys(Callbacks).includes(cbPrefix)) {
      throw new Error('Not available callback.')
    }

    return scenarious.get(cbPrefix)(ctx)
  }
}
