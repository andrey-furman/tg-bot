import Markup from 'telegraf/markup'

import { Handler } from './main.handler'
import { ContextMessageUpdate as Ctx, Contexts } from '../../shared'
import { dropboxUpload, getFilePath, keyboards } from '../../helpers'
import { Ads, Proposals } from '../../DB/models'
import { knex } from '../../DB'
import { session } from '../../bootstrap'
import papyrus from '../../papyrus'

const scenarious = new Map()

scenarious.set(Contexts.onAdPhotoUploading, async (ctx: Ctx) => {
  const {
    from: { id },
    update: {
      message: { photo },
    },
  } = ctx
  const [filePath, fileName] = await getFilePath(photo[0].file_id)
  await ctx.reply(papyrus.uploadingStart)
  let photo_url: any = await dropboxUpload(filePath, fileName)
  await ctx.reply(papyrus.uploadingEnd)
  // url could be duplicated. In this case photo_url == false. This means that
  // the photo url hasn't been changed. And an old photo url retrieves from session
  const {
    photoUrl,
    itemId: itemIdRaw,
    categoryId: categoryIdRaw,
    itemDescription,
    weight: weightRaw,
    regionId: regionIdRaw,
    phone,
  } = await session.getSession(id)
  // update photo url for existed item
  if (!photo_url) {
    photo_url = photoUrl
  } else {
    await session.update(id, { photoUrl: photo_url })
  }
  if (itemIdRaw) {
    const itemId = Number(itemIdRaw)
    await Proposals.query().update({ photo_url }).where('proposal_id', '=', itemId)
    // create a new item after photo uploading
  } else {
    // function available for sellers only
    const categoryId = Number(categoryIdRaw)
    const regionId = Number(regionIdRaw)
    const weight = Number(weightRaw)
    await Proposals.query().insert({
      category_id: categoryId,
      body: itemDescription,
      photo_url,
      weight,
      phone,
      region_id: regionId,
    })
  }
  await session.update(id, {
    context: Contexts.onMainMenu,
    itemId: '',
    weight: '',
    itemDescription: '',
    categoryId: '',
  })

  return await ctx.reply(papyrus.adIsAdded)
})

export class DocHandler extends Handler {
  constructor() {
    super()
  }

  async handle(ctx: Ctx) {
    const {
      from: { id },
    } = ctx
    const { context } = await session.getSession(id)
    if (!Object.keys(Contexts).includes(context)) {
      throw new Error('Not available context.')
    }

    return await scenarious.get(context)(ctx)
  }
}
