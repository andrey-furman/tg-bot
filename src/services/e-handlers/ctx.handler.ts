import Markup from 'telegraf/markup'
import Extra from 'telegraf/extra'

import { knex } from '../../DB'
import {
  ContextMessageUpdate as Ctx,
  Buttons,
  Contexts,
  Category,
  MsgScenarious,
} from '../../shared'
import { Ads, Categories, Proposals, Regions, Users } from '../../DB/models'
import { Handler } from './main.handler'
import {
  getProposalCard,
  inlineKeyboards,
  keyboards,
  updateSessionAndOutputProposalCards,
} from '../../helpers'
import {
  validateDescription,
  validateName,
  validatePhone,
  validateRegion,
  validateString,
  validateWeight,
} from '../../utils'
import { session } from '../../bootstrap'
import papyrus from '../../papyrus'

const scenarious = new Map()

scenarious.set(Contexts.onAskName, async (ctx: Ctx) => {
  const {
    from: { id },
    message: { text: name },
  } = ctx
  if (!validateName(name)) {
    return await ctx.reply(papyrus.errors.wrongPhone)
  }
  await session.update(id, { context: Contexts.onAskPhoneNumber })
  await ctx.reply(papyrus.greeting)
  return await ctx.reply(
    papyrus.askPhone,
    Extra.markdown().markup(markup => {
      return markup.resize().keyboard([markup.contactRequestButton(Buttons.sendMyPhone)])
    }),
  )
})

// for buyer region has been chosen
scenarious.set(Contexts.onSelectCategory, async (ctx: Ctx) => {
  const {
    from: { id },
    message: { text: region },
  } = ctx
  const { phone, type } = await session.getSession(id)
  const regionData = await Regions.query().where({ region }).first()
  let desc = papyrus.enterDescriptionSeller
  if (type === 'buyer') {
    desc = papyrus.enterDescriptionBuyer
  }
  await session.update(id, { regionId: regionData.region_id, context: Contexts.onEnterDescription })

  return await ctx.reply(desc)
})

scenarious.set(Contexts.onMainMenu, async (ctx: Ctx) => {
  const {
    from: { id, username },
    message: { text },
  } = ctx
  const { phone, type, username: sessionUsername } = await session.getSession(id)
  await session.update(id, { context: Contexts.onMainMenu })
  if (username && !sessionUsername) {
    await session.update(id, { username })
    await Users.query()
      .update({
        username,
      })
      .where({ phone })
  }

  if (text === Buttons.placeItem) {
    await session.update(id, { isProposalsMode: '', itemId: '' })
    if (type === 'seller') {
      // make an add
      const categories: Category[] = await Categories.query().select()
      return await ctx.reply(
        papyrus.selectCategory,
        Markup.inlineKeyboard(inlineKeyboards.categories(categories)).resize().extra(),
      )
    } else if (type === 'buyer') {
      await session.update(id, { context: Contexts.onSelectProposalRegion })
      const categories: Category[] = await Categories.query().select()
      return await ctx.reply(
        papyrus.selectCategory,
        Markup.inlineKeyboard(inlineKeyboards.categories(categories, true)).resize().extra(),
      )
    }
  } else if (text === Buttons.proposals) {
    const categories = await Categories.query().select()
    const { type } = await session.getSession(id)
    let inlineMode = false
    let context = Contexts.onMainMenu
    if (type === 'buyer') {
      context = Contexts.onSelectSellerProposalsRegion
      inlineMode = true
    }
    await session.update(id, { context, isProposalsMode: 1 })
    return await ctx.reply(
      papyrus.selectCategory,
      Markup.inlineKeyboard(inlineKeyboards.categories(categories, inlineMode)).resize().extra(),
    )
    // chose a category
  } else if (text === Buttons.announcements) {
    let items: any
    if (type === 'buyer') {
      items = await Ads.query()
        .select(
          knex.raw(
            'users.username, ads.ad_id as item_id, ads.body, ads.phone, ads.photo_url, ads.weight, ads.created_at, categories.category',
          ),
        )
        .leftJoin('users', 'users.phone', '=', 'ads.phone')
        .leftJoin('categories', 'categories.category_id', '=', 'ads.category_id')
        .where('ads.phone', '=', phone)
        .limit(100)
    } else {
      items = await Proposals.query()
        .select(
          knex.raw(
            'users.telegram_id, users.username, proposals.proposal_id as item_id, proposals.body, users.phone, proposals.weight, proposals.photo_url, proposals.created_at, categories.category',
          ),
        )
        .leftJoin('users', 'users.phone', '=', 'proposals.phone')
        .leftJoin('categories', 'categories.category_id', '=', 'proposals.category_id')
        .where('users.phone', '=', phone)
        .limit(100)
    }
    await session.update(id, { items: JSON.stringify(items), isProposalsMode: '' })
    const cardsData: any = getProposalCard(items, 0, '')
    if (!cardsData) {
      return await ctx.reply(papyrus.absentProposals)
    }
    const { photoUrl, inlineKeyboard, caption } = cardsData
    const itemUrl = photoUrl || papyrus.defaultProductImage
    await ctx.reply(papyrus.cardUsingInstruction)

    return await ctx.telegram.sendPhoto(
      id,
      { url: itemUrl, filename: "announcement" },
      {
        reply_markup: {
          inline_keyboard: inlineKeyboard,
        },
        caption,
        parse_mode: 'Markdown',
      },
    )
  }
})

scenarious.set(MsgScenarious.menu, async (ctx: Ctx) => {
  const {
    from: { id },
  } = ctx
  await session.update(id, {
    context: Contexts.onMainMenu,
    itemId: '',
    regionId: '',
    isProposal: '',
  })

  return await ctx.reply(papyrus.menu, Markup.keyboard(keyboards.menu).resize().extra())
})

scenarious.set(Contexts.onSelectSellerProposalsRegion, async (ctx: Ctx) => {
  const {
    from: { id },
    message: { text: region },
  } = ctx
  if (!validateRegion(region)) {
    return await ctx.reply(papyrus.errors.wrongRegion)
  }
  const regionData: any = await Regions.query().where({ region }).first()
  if (!regionData) {
    return await ctx.reply(papyrus.errors.wrongRegion)
  }
  const { categoryId } = await session.getSession(id)
  await session.update(id, { categoryId: regionData.region_id, context: Contexts.onMainMenu })
  // show proposals for buyer
  const items = await Proposals.query()
    .select(
      knex.raw(
        'users.telegram_id, users.username, users.region_id, proposals.proposal_id as item_id, proposals.body, proposals.photo_url, users.phone, proposals.weight, proposals.created_at, categories.category',
      ),
    )
    .leftJoin('users', 'users.phone', '=', 'proposals.phone')
    .leftJoin('categories', 'categories.category_id', '=', 'proposals.category_id')
    .where('proposals.category_id', '=', categoryId)
    .modify(qb => {
      // conditional adding
      if (regionData.region_id !== 27) {
        qb.where('region_id', '=', regionData.region_id)
      }
    })
  return await updateSessionAndOutputProposalCards(ctx, items, categoryId)
})

scenarious.set(Contexts.onSelectProposalRegion, async (ctx: Ctx) => {
  const {
    from: { id },
    message: { text: region },
  } = ctx
  const { type } = await session.getSession(id)
  const regionData = await Regions.query().where({ region }).first()
  let desc = papyrus.enterDescriptionSeller
  if (type === 'buyer') {
    desc = papyrus.enterDescriptionBuyer
  }
  if (!validateRegion(region) || !regionData) {
    return ctx.reply(papyrus.errors.wrongRegion)
  }
  await session.update(id, { regionId: regionData.region_id, context: Contexts.onEnterDescription })

  return ctx.reply(desc)
})

scenarious.set(Contexts.onEnterDescription, async (ctx: Ctx) => {
  const {
    from: { id },
    message: { text: description },
  } = ctx
  if (!validateDescription(description)) {
    return await ctx.reply(papyrus.errors.wrongDescription(10))
  }
  const { itemId: itemIdRaw, type } = await session.getSession(id)
  const itemId = Number(itemIdRaw)
  if (itemIdRaw) {
    if (type === 'seller') {
      await Proposals.query().update({ body: description }).where('proposal_id', '=', itemId)
    } else {
      await Ads.query().update({ body: description }).where('ad_id', '=', itemId)
    }

    await session.update(id, { context: Contexts.onMainMenu, itemId: '' })
    return await ctx.reply(papyrus.menu)
  } else {
    await session.update(id, { context: Contexts.onEnterWeight, itemDescription: description })
  }
  // await ctx.editMessageReplyMarkup({ inline_keyboard: [] })

  return await ctx.reply(papyrus.enterWeight)
})

scenarious.set(Contexts.onEnterWeight, async (ctx: Ctx) => {
  const {
    from: { id },
    message: { text: weightRaw },
  } = ctx
  const weight = validateWeight(weightRaw)
  if (!weight) {
    return await ctx.reply(papyrus.errors.wrongWeight)
  }
  const {
    itemId: itemIdRaw,
    type,
    itemDescription,
    phone,
    categoryId: categoryIdRaw,
  } = await session.getSession(id)
  if (itemIdRaw) {
    const itemId = Number(itemIdRaw)
    if (type === 'seller') {
      await Proposals.query()
        .update({ weight: Number(weight) })
        .where('proposal_id', '=', itemId)
    } else {
      await Ads.query()
        .update({ weight: Number(weight) })
        .where('ad_id', '=', itemId)
    }
  } else {
    if (type === 'seller') {
      // redirect to photo
      await session.update(id, { context: Contexts.onAdPhotoUploading, weight })
      return await ctx.reply(papyrus.uploadPhoto)
    } else {
      const categoryId = Number(categoryIdRaw)
      await Ads.query().insert({
        body: itemDescription,
        phone,
        category_id: categoryId,
        weight: Number(weight),
      })
      await ctx.reply(papyrus.adIsAdded)
      // create ads
    }
  }

  await session.update(id, {
    context: Contexts.onMainMenu,
    itemId: '',
    weight: '',
    itemDescription: '',
  })
  return await ctx.reply(papyrus.menu)
})

scenarious.set(Contexts.onAskPhoneNumber, async (ctx: Ctx) => {
  const {
    from: { id },
    message: { text: phoneRaw },
  } = ctx

  return await ctx.reply(papyrus.errors.contactOnly)

  const phone = validatePhone(phoneRaw)
  if (!phone) {
    return await ctx.reply(papyrus.errors.wrongPhone)
  }
  await session.update(id, { context: Contexts.onSelectClientType, phone })
  await ctx.reply(papyrus.accepted, Markup.removeKeyboard().extra())
  return await ctx.reply(
    papyrus.selectAccountType,
    Markup.inlineKeyboard(inlineKeyboards.clientType()).resize().extra(),
  )
})

scenarious.set(Contexts.onEnterBroadcastMessage, async (ctx: Ctx) => {
  const {
    from: { id },
    message: { text: broadcastMessage },
  } = ctx
  if (!validateString(broadcastMessage, 5)) {
    return await ctx.reply(papyrus.errors.wrongLength(5))
  }
  await ctx.reply(papyrus.broadcastStarted)
  const users = await Users.query().select()
  try {
    users.map(async user => {
      await ctx.telegram.sendMessage(user.chat_id, broadcastMessage)
    })
  } catch (err) {
    console.error(err.message)
  }
  await session.update(id, { context: Contexts.onMainMenu })

  return await ctx.reply(papyrus.broadcastFinished)
})

scenarious.set(Contexts.onSelectClientType, async (ctx: Ctx) => {
  const {
    from: { id },
    message: { text: region },
  } = ctx
  const regionData = await Regions.query().where({ region }).first()
  const regionId = regionData ? regionData.region_id : 0
  const { phone, username, firstName, type, chatId } = await session.getSession(id)
  await Users.query().insert({
    phone,
    type: type || 'seller',
    first_name: firstName || '',
    username: username || '',
    telegram_id: id,
    chat_id: Number(chatId) || 0,
    region_id: regionId,
  })
  await session.update(id, { context: Contexts.onMainMenu })

  return await ctx.reply(papyrus.menu, Markup.keyboard(keyboards.menu).resize().extra())
})

export class CtxHandler extends Handler {
  constructor() {
    super()
  }

  async handle(ctx: Ctx, context: string) {
    const {
      message: { text },
    } = ctx
    if (text === Buttons.toMenu) {
      return await scenarious.get(MsgScenarious.menu)(ctx)
    }
    if (!Object.keys(Contexts).includes(context)) {
      throw new Error('Not available context.')
    }

    return await scenarious.get(context)(ctx)
  }
}
