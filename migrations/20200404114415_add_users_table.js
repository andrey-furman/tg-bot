exports.up = function (knex) {
  return Promise.all([
    knex.schema.createTable('users', t => {
      t.increments('user_id').unsigned().primary()
      t.string('username').notNull()
      t.string('telegram_id').notNull()
      t.integer('phone').unsigned().notNull()
      t.enu('type', ['seller', 'buyer']).notNullable().default('seller')
      t.dateTime('created_at').notNull().defaultTo(knex.fn.now())
      t.dateTime('updated_at').nullable()
    }),
    knex.schema.createTable('ads', t => {
      t.increments('ad_id').unsigned().primary()
      t.integer('category_id')
        .unsigned()
        .notNull()
        .references('category_id')
        .inTable('categories')
        .onUpdate('CASCADE')
        .index()
      t.integer('user_id')
        .unsigned()
        .notNull()
        .references('user_id')
        .inTable('users')
        .onUpdate('CASCADE')
        .index()
      t.string('body').notNull()
      t.enu('status', ['active', 'inactive']).notNullable().default('active')
      t.dateTime('created_at').notNull().defaultTo(knex.fn.now())
      t.dateTime('updated_at').nullable()
    }),
    knex.schema.createTable('proposals', t => {
      t.increments('proposal_id').unsigned().primary()
      t.integer('category_id')
        .unsigned()
        .notNull()
        .references('category_id')
        .inTable('categories')
        .onUpdate('CASCADE')
        .index()
      t.integer('region_id')
        .unsigned()
        .notNull()
        .references('region_id')
        .inTable('regions')
        .onUpdate('CASCADE')
        .index()
      t.integer('user_id')
        .unsigned()
        .notNull()
        .references('user_id')
        .inTable('users')
        .onUpdate('CASCADE')
        .index()
      t.string('photo_url').nullable()
      t.string('body').notNull()
      t.enu('status', ['active', 'inactive']).notNullable().default('active')
      t.dateTime('created_at').notNull().defaultTo(knex.fn.now())
      t.dateTime('updated_at').nullable()
    }),
    knex.schema.createTable('regions', t => {
      t.increments('region_id').unsigned().primary()
      t.string('region').notNull()
    }),
    knex.schema.createTable('categories', t => {
      t.increments('category_id').unsigned().primary()
      t.string('category').notNull()
      t.enu('status', ['active', 'inactive']).notNullable().default('active')
    }),
  ])
}

exports.down = function (knex) {
  return Promise.all([
    knex.schema.dropTable('users'),
    knex.schema.dropTable('ads'),
    knex.schema.dropTable('proposals'),
    knex.schema.dropTable('categories'),
    knex.schema.dropTable('regions'),
  ])
}
