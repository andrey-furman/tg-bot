import Markup from 'telegraf/markup'
import Extra from 'telegraf/extra'

import { Handler } from './main.handler'
import { keyboards } from '../../helpers'
import { session } from '../../bootstrap'
import { Users } from '../../DB/models'
import { ContextMessageUpdate as Ctx, Commands, Contexts } from '../../shared'
import papyrus from '../../papyrus'

const scenarious = new Map()

scenarious.set(Commands.flushSession, async (ctx: Ctx) => {
  try {
    await session.flushSession()

    return await ctx.reply(papyrus.sessionIsFlushed)
  } catch (err) {
    console.error(err)
  }
})

scenarious.set(Commands.start, async (ctx: Ctx) => {
  const {
    from: { id, first_name, username },
    message: {
      chat: { id: chatId },
    },
  } = ctx
  await session.checkout(id)
  // check if the user is new
  const user = await Users.query().select().where({ telegram_id: id }).first()
  const { firstName, username: sessionUsername } = await session.getSession(id)

  // update username and first name on case user filled it lately
  if (!firstName && first_name) {
    await session.update(id, { firstName: first_name })
  }
  if (!sessionUsername && username) {
    // add to output message notification
    await session.update(id, { username })
  }
  if (user) {
    const { phone, type, username: name } = user
    if (phone) {
      await session.update(id, { context: Contexts.onMainMenu, phone, type, name })
      return await ctx.reply(papyrus.menu, Markup.keyboard(keyboards.menu).resize().extra())
    }
    if (!first_name) {
      await session.update(id, {
        context: Contexts.onAskName,
        telegramId: id,
      })
      return await ctx.reply(
        papyrus.askName,
        Extra.markup(markup => markup.removeKeyboard()),
      )
    }
  }
  // depends on the username availability emit different resp message
  await session.update(id, { telegramId: id, context: Contexts.onAskPhoneNumber, chatId })
  setTimeout(() => {}, 100)
  return await ctx.reply(
    papyrus.greeting,
    Extra.markdown().markup(markup => {
      return markup.resize().keyboard([markup.contactRequestButton(papyrus.sendPhone)])
    }),
  )
})

scenarious.set(Commands.makeBroadcast, async (ctx: Ctx) => {
  const {
    from: { id },
  } = ctx
  await session.update(id, { context: Contexts.onEnterBroadcastMessage })

  return ctx.reply(papyrus.enterBroadcastMessage)
})

export class CmdHandler extends Handler {
  constructor() {
    super()
  }

  async handle(ctx: Ctx, commandEntity: string) {
    const command = commandEntity.replace('/', '')
    if (!Object.keys(Commands).includes(command)) {
      throw new Error('Command is not available.')
    }
    await scenarious.get(command)(ctx)
  }
}
