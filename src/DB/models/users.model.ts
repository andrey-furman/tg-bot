import { Model } from '..'
import { Proposals } from './proposals.model'
import { Ads } from './ads.model'

export class Users extends Model {
  username: string
  first_name: string
  telegram_id!: number
  region_id: number
  phone!: string
  chat_id: number
  type!: string
  created_at: Date
  updated_at: Date

  static get tableName() {
    return 'users'
  }

  static get idColumn() {
    return 'phone'
  }

  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        username: { type: 'string' },
        first_name: { type: 'string' },
        telegram_id: { type: 'integer' },
        chat_id: { type: 'integer' },
        region_id: { type: 'integer' },
        phone: { type: 'string' },
        type: { type: 'string', enum: ['seller', 'buyer'], default: 'seller' },
        created_at: { type: 'date' },
        updated_at: { type: 'date' },
      },
    }
  }

  $beforeInsert() {
    this.created_at = new Date()
  }

  $beforeUpdate() {
    this.updated_at = new Date()
  }

  static get relationMappings() {
    return {
      proposals: {
        relation: Model.HasManyRelation,
        modelClass: Proposals,
        join: {
          from: 'users.phone',
          to: 'proposals.phone',
        },
      },
      ads: {
        relation: Model.HasManyRelation,
        modelClass: Ads,
        join: {
          from: 'users.phone',
          to: 'ads.phone',
        },
      },
    }
  }
}
