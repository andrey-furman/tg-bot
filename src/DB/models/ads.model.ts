import { Model } from '../'

import { Users, Categories } from '../models'

export class Ads extends Model {
  ad_id: number
  phone: string
  body: string
  weight: number
  category_id: number
  photo_url?: string
  created_at: Date
  updated_at: Date

  static get tableName() {
    return 'ads'
  }

  static get idColumn() {
    return 'ad_id'
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: [],
      properties: {
        ad_id: { type: 'integer' },
        category_id: { type: 'integer' },
        phone: { type: 'string' },
        weight: { type: 'decimal', default: 0 },
        body: { type: 'string' },
        photo_url: { type: 'string' },
        status: { type: 'string', enum: ['active', 'inactive'], default: 'active' },
        created_at: { type: 'date' },
        updated_at: { type: 'date' },
      },
    }
  }

  $beforeInsert() {
    this.created_at = new Date()
  }

  $beforeUpdate() {
    this.updated_at = new Date()
  }

  static get relationMappings() {
    return {
      users: {
        relation: Model.BelongsToOneRelation,
        modelClass: Users,
        join: {
          from: 'ads.phone',
          to: 'users.phone',
        },
      },
      categories: {
        relation: Model.BelongsToOneRelation,
        modelClass: Categories,
        join: {
          from: 'ads.category_id',
          to: 'categories.category_id',
        },
      },
    }
  }
}
