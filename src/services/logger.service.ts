import axios from 'axios'

const baseURL = process.env.TELEGRAM_BASE_URL
const httpClientConfig = { headers: { 'Content-Type': 'application/json' } }

export class LoggerService {
  public static async logGroupMessage(message: string) {
    return await axios.post(
      `${baseURL}/sendMessage`,
      { chat_id: process.env.GROUP_CHAT_ID, text: message },
      httpClientConfig,
    )
  }

  public static async logGroupPhoto(photoUrl: string): Promise<void> {
    return await axios.post(
      `${baseURL}/sendMessage`,
      { chat_id: process.env.GROUP_CHAT_ID, photo: photoUrl },
      httpClientConfig,
    )
  }

  public static async logGroupDocument(dirPath: string, fileName: string) {}
}
