exports.up = function (knex) {
  return Promise.all([
    knex.schema.alterTable('ads', t => {
      t.string('photo_url', 500).nullable()
    }),
  ])
}

exports.down = function (knex) {
  return Promise.all([
    knex.schema.alterTable('ads', t => {
      t.dropColumn('photo_url')
    }),
  ])
}
