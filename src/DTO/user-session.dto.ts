export class InitUserSession {
  constructor(
    public cardItemIndex = '',
    public categoryId ='',
    public context = '',
    public chatId = '',
    public firstName = '',
    public isProposalsMode = '',
    public itemDescription = '',
    public itemId = '',
    public items = '',
    public name = '',
    public phone = '',
    public photoUrl = '',
    public proposalsRegion = '',
    public regionId = '',
    public telegramId = '',
    public type = '',
    public username = '',
    public weight = '',
  ) {}
}

export class CheckedOutSessionInst {
  constructor(
    public cardItemIndex = '',
    public categoryId = '',
    public context = '',
    public chatId = '',
    public firstName = '',
    public isProposalsMode = '',
    public itemDescription = '',
    public itemId = '',
    public items = '',
    public proposalsRegion = '',
    public regionId = '',
    public telegramId = '',
    public weight = '',
  ) {}
}
