import { Model } from '..'

import { Users, Categories, Regions } from '../models'

export class Proposals extends Model {
  proposal_id: number
  phone: string
  category_id: number
  body!: string
  region_id?: number
  weight: number
  photo_url: string
  created_at: Date
  updated_at: Date

  static get tableName() {
    return 'proposals'
  }

  static get idColumn() {
    return 'proposal_id'
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: [],
      properties: {
        body: { type: 'string' },
        category_id: { type: 'integer' },
        created_at: { type: 'date' },
        photo_url: { type: 'string' },
        weight: { type: 'decimal' },
        proposal_id: { type: 'integer' },
        region_id: { type: 'integer' },
        status: { type: 'string', enum: ['active', 'inactive'], default: 'active' },
        updated_at: { type: 'date' },
        phone: { type: 'string' },
      },
    }
  }

  $beforeInsert() {
    this.created_at = new Date()
  }

  $beforeUpdate() {
    this.updated_at = new Date()
  }

  static get relationMappings() {
    return {
      users: {
        relation: Model.BelongsToOneRelation,
        modelClass: Users,
        join: {
          from: 'proposals.phone',
          to: 'users.phone',
        },
      },
      categories: {
        relation: Model.BelongsToOneRelation,
        modelClass: Categories,
        join: {
          from: 'proposals.category_id',
          to: 'categories.category_id',
        },
      },
      regions: {
        relation: Model.BelongsToOneRelation,
        modelClass: Regions,
        join: {
          from: 'proposals.region_id',
          to: 'regions.region_id',
        },
      },
    }
  }
}
