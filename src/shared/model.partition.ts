export interface User {
}

export interface Category {
  category_id: number
  category: string
  status: string
}

export interface Region {
  region_id: number
  region: string
}