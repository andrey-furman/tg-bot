exports.up = function (knex) {
  return Promise.all([
    knex.schema.alterTable('users', t => {
      t.integer('region_id')
        .unsigned()
        .notNull()
        .references('region_id')
        .inTable('regions')
        .onUpdate('CASCADE')
        .index()
    }),
  ])
}

exports.down = function (knex) {
  return Promise.all([
    knex.schema.alterTable('users', t => {
      t.dropColumn('region_id')
    }),
  ])
}
