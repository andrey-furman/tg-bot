exports.up = function (knex) {
  return Promise.all([
    knex.schema.alterTable('users', t => {
      t.integer('phone').unique().unsigned().primary()
    }),
  ])
}

exports.down = function (knex) {
  return Promise.all([
    knex.schema.alterTable('users', t => {
      t.dropColumn('phone')
    }),
  ])
}
