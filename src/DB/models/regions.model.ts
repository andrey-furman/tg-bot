import { Model } from '../'
import { Users, Proposals } from '../models'

export class Regions extends Model {
  region_id: number
  region: string

  static get tableName() {
    return 'regions'
  }

  static get idColumn() {
    return 'region_id'
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['region'],
      properties: {
        region_id: { type: 'integer' },
        region: { type: 'string' },
      },
    }
  }

  static get relationMappings() {
    return {
      proposals: {
        relation: Model.HasManyRelation,
        modelClass: Proposals,
        join: {
          from: 'regions.region_id',
          to: 'proposals.region_id',
        },
      },
    }
  }
}
