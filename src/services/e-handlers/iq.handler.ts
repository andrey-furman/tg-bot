import { Handler } from './main.handler'
import { Regions } from '../../DB/models'
import { ContextMessageUpdate as Ctx, InlineQueries, Contexts, Category } from '../../shared'
import { getIdAndQueryParam, getQueryList, inlineKeyboards } from '../../helpers'
import { sortRegions } from '../../utils'
import { session } from '../../bootstrap'
import papyrus from '../../papyrus'

const scenarious = new Map()

scenarious.set(InlineQueries.onClientTypeSelected, async (ctx: Ctx) => {
  const [id, type] = getIdAndQueryParam(ctx)
  await session.update(id, { type: 'seller' })
  const regionsAll = await Regions.query().select().orderBy('region', 'asc')
  const regions = regionsAll.filter(({ region }) => region !== 'Вся Украина')
  const regionsList: any = getQueryList(regions)
  await session.update(id, { type })
  return await ctx.answerInlineQuery(regionsList, {
    cache_time: 0,
  })
})

scenarious.set(InlineQueries.onEnterBuyerProposal, async (ctx: Ctx) => {
  const [id, categoryIdRaw] = getIdAndQueryParam(ctx)
  await session.update(id, { categoryId: Number(categoryIdRaw) })
  const regionsUnsorted: any = await Regions.query().select().orderBy('region', 'asc')
  const regions = sortRegions(regionsUnsorted)
  const regionsList: any = getQueryList(regions)
  return await ctx.answerInlineQuery(regionsList, {
    cache_time: 0,
  })
})

export class IQHandler extends Handler {
  constructor() {
    super()
  }

  handle(ctx: Ctx) {
    const queryRaw = ctx.inlineQuery.query
    const [query, param] = queryRaw.split(':')
    if (!Object.keys(InlineQueries).includes(query)) {
      throw new Error('Not available callback.')
    }

    return scenarious.get(query)(ctx)
  }
}
