import Markup from 'telegraf/markup'

import { ContextMessageUpdate as Ctx, Contexts } from './shared'
import { bot, session } from './bootstrap'
import {
  CmdHandler,
  CtxHandler,
  MsgHandler,
  CbHandler,
  IQHandler,
  DocHandler,
} from './services/e-handlers'
import { validatePhone } from './utils'
import { inlineKeyboards } from './helpers'
import papyrus from './papyrus'

const cmdH = new CmdHandler()
const ctxH = new CtxHandler()
const msgH = new MsgHandler()
const cbH = new CbHandler()
const iqH = new IQHandler()
const docH = new DocHandler()

const messageDelegator = async (ctx: Ctx, context: '') => {
  const msg = ctx.message.text
  const commandRegex = /^\/\w*/
  if (msg.match(commandRegex)) {
    return await cmdH.handle(ctx, msg)
  }
  if (context) {
    return await ctxH.handle(ctx, context)
  }
  return await msgH.handle(ctx)
}

bot.start(
  async (ctx: Ctx): Promise<any> => {
    try {
      return await messageDelegator(ctx, '')
    } catch (err) {
      console.error(err)
    }
  },
)

bot.on(['document', 'photo'], async (ctx: Ctx) => {
  try {
    return await docH.handle(ctx)
  } catch (err) {
    console.error(err)
  }
})

bot.on('contact', async (ctx: Ctx) => {
  const {
    from: { id },
    message: {
      contact: { phone_number },
    },
  } = ctx
  const phone = validatePhone(phone_number)
  if (!phone) {
    return await ctx.reply(papyrus.errors.wrongPhone)
  }
  await session.update(id, { context: Contexts.onSelectClientType, phone })
  await ctx.reply(papyrus.accepted, Markup.removeKeyboard().extra())
  return await ctx.reply(
    papyrus.selectAccountType,
    Markup.inlineKeyboard(inlineKeyboards.clientType()).resize().extra(),
  )
})

bot.on('message', async (ctx: Ctx) => {
  try {
    const {
      from: { id },
    } = ctx
    const { context } = await session.getData(id, ['context'])
    return await messageDelegator(ctx, context)
  } catch (err) {
    console.error(err)
  }
})

bot.on('inline_query', async (ctx: Ctx) => {
  try {
    return await iqH.handle(ctx)
  } catch (err) {
    console.error(err)
  }
})

bot.on(
  'callback_query',
  async (ctx: Ctx): Promise<any> => {
    try {
      return await cbH.handle(ctx)
    } catch (err) {
      console.error(err)
    }
  },
)
