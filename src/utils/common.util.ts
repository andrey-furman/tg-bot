import { Region } from '../shared'

export const isJSON = (toParse: string) => {
  return Boolean(JSON.parse(toParse))
}

export const stringifyMap = (map: Record<string, any>): string => {
  return JSON.stringify(Array.from(map.entries()))
}

export const reverseMap = (json: string): Record<string, any> => {
  return new Map(JSON.parse(json))
}

export const getScenarioNameByText = (eType, text: any): string => {
  const keys = Object.keys(eType)
  const values: string[] = Object.values(eType)
  const idx = values.indexOf(text)
  return keys[idx] || ''
}

export const sortRegions = (regions: Region[]): Region[] => {
  const ukraineRegion = regions.find(region => region.region === 'Вся Украина')
  const ukraineRegionIndex = regions.indexOf(ukraineRegion)
  const shuffledDeck = shuffleDeck(regions, ukraineRegionIndex, 0)

  return shuffledDeck
}

const shuffleDeck = (arr, oldIndex, newIndex) => {
  if (newIndex >= arr.length) {
    var k = newIndex - arr.length + 1
    while (k--) {
      arr.push(undefined)
    }
  }
  arr.splice(newIndex, 0, arr.splice(oldIndex, 1)[0])
  return arr
}
