import axios from 'axios'

import { Ads, Proposals } from '../DB/models'

const crontabs = new Map()

// Once a day check for outdated items (older than 7 days)
crontabs.set('removeOutdatedItems', async () => {
  try {
    setInterval(async () => {
      await Ads.query().delete().whereRaw("created_at < NOW() - INTERVAL '7 days'")
      await Proposals.query().delete().whereRaw("created_at < NOW() - INTERVAL '7 days'")
    }, 86400000)
  } catch (err) {
    console.error(err.message)
  }
})

export class SchedulerService {
  public static init() {
    crontabs.forEach(crontab => crontab())
  }
}
