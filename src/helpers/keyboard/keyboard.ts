import { Buttons } from '../../shared'

export const keyboards = {
  menu: [[Buttons.placeItem, Buttons.proposals, Buttons.announcements], [Buttons.toMenu]],
}
