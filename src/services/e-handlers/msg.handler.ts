import Markup from 'telegraf/markup'
import Extra from 'telegraf/extra'

import { Handler } from './main.handler'
import { ContextMessageUpdate as Ctx, Buttons, MsgScenarious } from '../../shared'
import { Regions } from '../../DB/models'
import { getScenarioNameByText } from '../../utils'
import { session } from '../../bootstrap'
import papyrus from '../../papyrus'

const scenarious = new Map()

export class MsgHandler extends Handler {
  async handle(ctx: Ctx) {
    const {
      message: { text },
    } = ctx
    const scenarioName = getScenarioNameByText(Buttons, text)
    if (!Object.keys(Buttons).includes(scenarioName)) {
      throw new Error('Now available scenario.')
    }
    return await scenarious.get(scenarioName)(ctx)
  }
}
