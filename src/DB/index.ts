import Knex from 'knex'
import dotenv from 'dotenv'
import { Model } from 'objection'

dotenv.config()

const knex = Knex({ client: 'pg', connection: process.env.POSTGRES_URI })
Model.knex(knex)

export { Model, knex }
