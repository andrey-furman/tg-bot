import { ContextMessageUpdate as Ctx } from '../../shared'

export abstract class Handler {
  public abstract handle(ctx: Ctx, entity: string)
}
