export const checkName = (name: string): boolean => {
  if (!name) {
    return false
  }
  if (name.length < 3) {
    return false
  }

  return true
}

export const validateName = (name: string): boolean => {
  if (!name) return false
  if (name.length < 3) return false

  return true
}

export const validateDescription = (desc: string): boolean => {
  if (desc.length < 10) {
    return false
  }

  return true
}

export const validateWeight = (weightRaw: string): boolean | number => {
  const weight = Number(weightRaw.replace(',', '.'))
  if (!weight) {
    return false
  }

  if (weight < 0.1 || weight > 1000) {
    return false
  }

  return weight
}

export const validateString = (string: string, length: number): boolean => {
  if (string.length < length) {
    return false
  }

  return true
}

export const validateRegion = (region: string): boolean => {
  if (region.length < 5) {
    return false
  }

  return true
}

export const validatePhone = (phoneRaw: string): boolean | string => {
  if (!phoneRaw) {
    return false
  }
  const phone = phoneRaw.replace(/[^0-9.]/g, '')
  if (String(phone).length < 10 || String(phone).length > 13) {
    return false
  }

  return phone
}
