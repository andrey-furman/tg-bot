exports.up = function (knex) {
  return Promise.all([
    knex.schema.alterTable('proposals', t => {
      t.integer('phone')
        .unique()
        .notNull()
        .references('phone')
        .inTable('users')
        .onUpdate('CASCADE')
        .index()
    }),
    knex.schema.alterTable('ads', t => {
      t.integer('phone')
        .unique()
        .notNull()
        .references('phone')
        .inTable('users')
        .onUpdate('CASCADE')
        .index()
    }),
  ])
}

exports.down = function (knex) {}
