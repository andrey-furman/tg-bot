export { Context as ContextMessageUpdate } from '../../node_modules/telegraf'
export { InlineKeyboardButton } from '../../node_modules/telegraf/telegraf'
export * from './model.partition'

export enum Commands {
  start = 'start',
  flushSession = 'flushSession',
  makeBroadcast = 'makeBroadcast',
}

export enum Contexts {
  onAskPhoneNumber = 'onAskPhoneNumber',
  onEnterDescription = 'onEnterDescription',
  onEditItemMenu = 'onEditMenu',
  onAskName = 'onAskName',
  onEnterBroadcastMessage = 'onEnterBroadcastMessage',
  onSelectClientType = 'onSelectClientType',
  onSelectCategory = 'onSelectCategory',
  onSelectProposalRegion = 'onSelectProposalRegion',
  onEnterWeight = 'onEnterWeight',
  onMainMenu = 'onMainMenu',
  onWatchBuyerProposals = 'onWatchBuyerProposals',
  onWatchSellerProposals = 'onWatchSellerProposals',
  onAdPhotoUploading = 'onAdPhotoUploading',
  onSelectSellerProposalsRegion = 'onSelectSellerProposalsRegion',
}

export enum Callbacks {
  onPlateSelected = 'onPlateSelected',
  onClientTypeSelected = 'onClientTypeSelected',
  onSelectCategory = 'onSelectCategory',
  onChangeCard = 'onChangeCard',
  onEditItem = 'onEditItem',
  onEditDesc = 'onEditDesc',
  onEditPhoto = 'onEditPhoto',
  onEditWeight = 'onEditWeight',
  onDeleteItem = 'onDeleteItem',
  onRefuseAction = 'onRefuseAction',
  onGetContact = 'onGetContact',
  back = 'back',
  onConfirmAction = 'onConfirmAction',
  onDeclineAction = 'onDeclineAction',
}

export enum InlineQueries {
  onClientTypeSelected = 'onClientTypeSelected',
  onEnterBuyerProposal = 'onEnterBuyerProposal',
}

export enum MsgScenarious {
  buyer = 'buyer',
  seller = 'seller',
  placeItem = 'placeItem',
  announcements = 'announcements',
  proposals = 'proposals',
  sendMyPhone = 'sendMyPhone',
  previous = 'previous',
  next = 'next',
  menu = 'menu',
  back = 'back',
}

export enum Buttons {
  previous = '⬅️️',
  next = '➡️',
  buyer = '🛒Покупатель',
  seller = '📦Продавец',
  editDesc = 'Описание',
  editWeight = 'Вес',
  editPhoto = 'Фото',
  placeItem = '📢Дать объявление',
  proposals = '🧾Предложения',
  announcements = '📑Мои объявления',
  sendMyPhone = '📲Отправить номер',
  getContact = '📱Отрмати контакт',
  toMenu = '⌂В меню',
  back = '⬅️Назад',
}

export interface CardData {
  photoUrl: string
  inlineKeyboard: any
  caption: string
}

export interface ProposalKeyboardData {
  username: string
  cur: { position: string }
  next: { index: number }
  prev: { index: number }
  isFirst: boolean
  isLast: boolean
}
