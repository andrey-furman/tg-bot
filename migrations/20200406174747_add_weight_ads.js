exports.up = function (knex) {
  return Promise.all([
    knex.schema.alterTable('ads', t => {
      t.decimal('weight').notNull()
    }),
  ])
}

exports.down = function (knex) {
  return Promise.all([
    knex.schema.alterTable('ads', t => {
      t.dropColumn('weight')
    }),
  ])
}
