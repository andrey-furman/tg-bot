exports.up = function (knex) {
  return Promise.all([
    knex.schema.alterTable('ads', t => {
      t.dropColumn('phone')
    }),
    knex.schema.alterTable('proposals', t => {
      t.dropColumn('phone')
    }),
    knex.schema.alterTable('users', t => {
      t.dropColumn('phone')
    }),
    knex.schema.alterTable('users', t => {
      t.string('phone', 15).unique().unsigned().primary()
    }),
    knex.schema.alterTable('proposals', t => {
      t.string('phone', 15)
        .unique()
        .notNull()
        .references('phone')
        .inTable('users')
        .onUpdate('CASCADE')
        .index()
    }),
    knex.schema.alterTable('ads', t => {
      t.string('phone', 15)
        .unique()
        .notNull()
        .references('phone')
        .inTable('users')
        .onUpdate('CASCADE')
        .index()
    }),
  ])
}

exports.down = function (knex) {}
