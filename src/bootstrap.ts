import Telegraf from 'telegraf'
import dotenv from 'dotenv'
import redis, { RedisClient } from 'redis'

import { SessionService } from './services/session.service'
import { SchedulerService } from './services/scheduler.service'

dotenv.config()

const bot = new Telegraf(process.env.BOT_TOKEN)
let session: SessionService
let redisClient: RedisClient
// initialize crontabs
SchedulerService.init()

// TODO  use async functions for both cases
async function setBotWebhook() {
  try {
    // 🔗 set bot webhook
    await bot.telegram.setWebhook(process.env.EXPOSE_URL)
    await bot.startWebhook('/', null, Number(process.env.PORT))
    console.log('Bot started on Port:', process.env.PORT)
  } catch (err) {
    console.log('err on webhook', err)
  }
}

async function initRedisClient() {
  // 📮 init redis client
  return await redis.createClient({
    url: process.env.REDIS_URL,
    retry_strategy: options => {
      if (options.error && options.error.code === 'ECONNREFUSED') {
        return new Error('Redis server access refused.')
      }
      if (options.total_retry_time > 1000 * 60 * 60) {
        return new Error('Retry time exhausted')
      }
      if (options.attempt > 10) {
        return undefined
      }
      // reconnect after
      return Math.min(options.attempt * 100, 3000)
    },
  })
}

// 🎬 Bootstrap
try {
  ;(async () => {
    await setBotWebhook()
    redisClient = await initRedisClient()
    session = new SessionService(redisClient)
  })()
} catch (err) {
  console.log('Error on bootstrap:', err)
  bot.telegram.deleteWebhook()
}

export { bot, session, redisClient }
