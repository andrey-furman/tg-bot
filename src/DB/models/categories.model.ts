import { Model } from '..'
import { Proposals } from './proposals.model'
import { Ads } from './ads.model'

export class Categories extends Model {
  category_id: number
  category: string
  status: string
  updated_at: Date
  created_at: Date

  static get tableName() {
    return 'categories'
  }

  static get idColumn() {
    return 'category_id'
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['category'],
      properties: {
        category_id: { type: 'integer' },
        category: { type: 'string' },
        status: { type: 'string', enum: ['active', 'inactive'], default: 'active' },
      },
    }
  }

  static get relationMappings() {
    return {
      ads: {
        relation: Model.HasManyRelation,
        modelClass: Ads,
        join: {
          from: 'categories.category_id',
          to: 'ads.category_id',
        },
      },
      proposals: {
        relation: Model.HasManyRelation,
        modelClass: Proposals,
        join: {
          from: 'categories.category_id',
          to: 'proposals.category_id',
        },
      },
    }
  }
}
