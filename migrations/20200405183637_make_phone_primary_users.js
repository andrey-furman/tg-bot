exports.up = function (knex) {
  return Promise.all([
    knex.schema.alterTable('ads', t => {
      t.dropForeign('user_id')
      t.dropColumn('user_id')
    }),
    knex.schema.alterTable('proposals', t => {
      t.dropForeign('user_id')
      t.dropColumn('user_id')
    }),
  ])
}

exports.down = function (knex) {}
