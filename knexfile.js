const dotenv = require('dotenv')

dotenv.config()

module.exports = {
  development: {
    client: 'postgresql',
    connection: process.env.POSTGRES_URI,
  },
  production: {
    client: 'postgresql',
    connection: process.env.POSTGRES_URI,
  },
}
